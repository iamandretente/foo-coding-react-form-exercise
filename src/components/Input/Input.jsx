import React from "react"
import PropTypes from "prop-types"
import "./input.css"

export default function Input(props) {
  return (
    <div>
      <label className="label">{props.label}</label>
      <input type={props.type} />
    </div>
  )
}

Input.propTypes = {
  label: PropTypes.string,
  type: PropTypes.string,
}
