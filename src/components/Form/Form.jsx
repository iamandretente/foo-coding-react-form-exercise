import React, { useState } from "react"
import PropTypes from "prop-types"
import Input from "../Input/Input"
import Button from "../Button/Button"
import "./form.css"

export default function Form(props) {
  const [passwordInputType, setPasswordInputType] = useState("password")

  function onClickShowPassword(event) {
    event.preventDefault()

    if (passwordInputType === "password") {
      setPasswordInputType("text")
    } else {
      setPasswordInputType("password")
    }
  }

  return (
    <div>
      <h1>{props.title}</h1>

      <form>
        <Input label="Name" type="text" />
        <Input label="Email" type="email" />
        <Input label="Password" type={passwordInputType} />
        <Input label="Confirm password" type={passwordInputType} />

        <Button
          className="form__button-show-password"
          label="Show password"
          onClick={onClickShowPassword}
        />

        <Button className="form__button-submit" label="Submit" />
      </form>
    </div>
  )
}

Form.propTypes = {
  title: PropTypes.string,
}
