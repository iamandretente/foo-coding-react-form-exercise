import React from "react"
import Form from "./components/Form/Form"
import "./App.css"

function App() {
  return (
    <div className="App">
      <Form title="Form title" />
    </div>
  )
}

export default App
